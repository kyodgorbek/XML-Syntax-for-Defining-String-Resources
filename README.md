# XML-Syntax-for-Defining-String-Resources
<resources>
    <string name="simple_string">simple string</string>
    <string name="quoted_string"> "quoted'string"</string>
    <string name="doublie_quoted_string">\"double\"</string>
    <string name="java_format_string">
                          hello %2$s java format string.  %1$s again
	</string>
	<string name="tagged_string">
	      Hello <b><i>Slanted Android</i>
	</string>
</resources>
